## Name
UMD_Ipelets

## Description
This is a project to assemble Ipelets for many general polygonal operations used by the field of convex geometry.
To read more please check our [arxiv post](https://arxiv.org/abs/2403.10033) and our [video](https://www.youtube.com/watch?v=bWBh-MkIMe4)! 

## Ipelets
We have several Ipelets available for use they are:
1. Minkowski Sum
2. Polar Body
3. Macbeath Region
5. Minimum Spanning Tree (minimum Funk, Hilbert, Euclidean)
6. Minimum Enclosing Ball
7. Hilbert Balls
8. Funk and Reverse Funk Balls

## Installation
Download the files, put them in the Ipelets subfolder folder in your Ipe folder.

## Usage
They should appear under Ipelets.

## Support
If there are issues email octavo@umd.edu

## Roadmap
We will continue to put up more Ipelets for basic computational geometry operations in the polygonal goeometry. 

## Authors and acknowledgment
Contributors are:
Nithin Parepally 
Ainesh Chatterjee
Sukrit Mangla
Hongyang Du
Kenny Wu
Sarah Hwang

## License
We are using the MIT License

## Project status
This is an ongoing project
